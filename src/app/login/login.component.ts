import { Component, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';

import { SessionData, UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  sessionData$!: Observable<SessionData>;

  constructor(private userService: UserService) {}
  ngOnInit() {
    this.sessionData$ = this.userService.sessionData$.asObservable();
  }

  share() {
    window.alert('The product has been shared!');
  }
}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/
