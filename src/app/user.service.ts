import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

export interface SessionData {
  logged_in: boolean;
  in_whitelist: boolean;
  in_bureau: boolean;
  authorized: boolean;
  username: string;
  auth_url: string;
  logout_url: string;
}

@Injectable({
  providedIn: 'root',
})
export class UserService {
  sessionData$ = new ReplaySubject<SessionData>(1);

  constructor(private http: HttpClient) {
    this.start();
  }

  start() {
    this.fetchSessionData().subscribe((data: SessionData) => {
      this.sessionData$.next({ ...data });
    });
  }

  fetchSessionData() {
    return this.http.get<SessionData>(
      '/session',
      { withCredentials: true }
    );
  }

  logout() {}

  login() {}
}
