import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { SessionData, UserService } from '../user.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css'],
})
export class TopBarComponent {
  sessionData$!: Observable<SessionData>;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.sessionData$ = this.userService.sessionData$.asObservable();
  }

  logOut() {}

  logIn() {}
}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/
