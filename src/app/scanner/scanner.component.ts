import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Html5QrcodeScanner, Html5QrcodeScanType } from "html5-qrcode";
import { Html5QrcodeResult } from "html5-qrcode/esm/core";

export interface BadgeData {
  abuse_last_24h: number,
  served_last_24h: number
}

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.css']
})
export class ScannerComponent implements AfterViewInit {

  html5QrcodeScanner!: Html5QrcodeScanner;

  scanning: boolean = true;
  started: boolean = false;
  serving: boolean = false;
  lastQRString: string | undefined;
  lastBadgeState: BadgeData | undefined;
  cameraId!: string


  constructor(private http: HttpClient) { }

  ngAfterViewInit() {
    this.initHtml5Qrcode();
    
  }

  fetchBadgeState(badge: string) {
      return this.http.get<BadgeData>(
        "/badge/" + badge,
        { withCredentials: true }
      );
  }

  serveBeer() {
    this.serving = true;
    this.http.post<any>(
      "/badge/" + this.lastQRString,
      { served: true },
      { withCredentials: true }
    ).subscribe(
      data => {
        // TODO: here check backend success
        this.serving = false;
        this.cancel();
      }
    )
  }

  cancel() {
    this.lastQRString = undefined;
    this.lastBadgeState = undefined;
    this.scanning = true;
  }


  onScanSuccess(decodedText: string, decodedResult: Html5QrcodeResult) {
    let validatedText = /^\w+$/.test(decodedText);
    if (this.scanning && validatedText) {
      this.scanning = false;
      this.lastQRString = decodedText;
      this.fetchBadgeState(decodedText).subscribe(
          (data: BadgeData) => (this.lastBadgeState = { ...data }) 
      )
    }
    if (!validatedText) {
      console.log('Decoded QR has invalid characters!')
    }
  }

  initHtml5Qrcode() {
    this.html5QrcodeScanner = new Html5QrcodeScanner(
      "reader",
      {
        fps: 10,
        qrbox: 250,
        supportedScanTypes: [ Html5QrcodeScanType.SCAN_TYPE_CAMERA ]
      },
      false  
    )
    this.html5QrcodeScanner.render(
      (decodedText, decodedResult) => {
        this.onScanSuccess(decodedText, decodedResult);
      },
      undefined
    )
  }
}
